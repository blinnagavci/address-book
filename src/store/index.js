import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // get contacts if they are previously stored in localStorage and if not, create new array.
    contacts: JSON.parse(localStorage.getItem('contacts')) || []
  },

  mutations: {
    // add new contact to list.
    addContact(state, payload) {
      state.contacts.push(payload);
      localStorage.setItem('contacts', JSON.stringify(state.contacts));
    },

    // update existing contact.
    editContact(state, payload) {
      const idx = state.contacts.findIndex(contact => contact.id == payload.id);
      state.contacts[idx] = payload;
      localStorage.setItem('contacts', JSON.stringify(state.contacts));
    },

    // remove contact from list.
    deleteContact(state, payload) {
      const idx = state.contacts.findIndex(contact => contact.id == payload)
      state.contacts.splice(idx, 1);
      localStorage.setItem('contacts', JSON.stringify(state.contacts));
    }
  },
  actions: {
    addContact(context, payload) {
      context.commit('addContact', payload);
    },

    editContact(context, payload) {
      context.commit('editContact', payload);
    },

    deleteContact(context, payload) {
      context.commit('deleteContact', payload)
    }
  },
  getters: {
    // returns contact list.
    getContactsList(state) {
      return state.contacts;
    },

    // returns specific contact based on the id.
    getSelectedContact: (state) => (id) => {
      return state.contacts.find(contact => contact.id == id)
    }
  }
})
