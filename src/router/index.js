import Vue from 'vue'
import VueRouter from 'vue-router'
import ContactsList from '../views/ContactsList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ContactsList',
    component: ContactsList
  },
  {
    path: '/contact-form/:id?',
    name: 'ContactForm',
    component: () => import('../views/ContactForm.vue')
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('../views/PageNotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
